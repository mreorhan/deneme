import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducers from "./reducers";
import registerServiceWorker from './registerServiceWorker';
//import { BrowserRouter as Router   } from 'react-router-dom';
ReactDOM.render(
<Provider store={createStore(reducers)}>
    <App />
  </Provider>
    , document.getElementById('root'));
registerServiceWorker();
